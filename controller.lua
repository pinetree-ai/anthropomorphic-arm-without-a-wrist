if init then
	t = 0

	C1 = pid_init(80,20,20)
	C2 = pid_init(80,20,20)
	C3 = pid_init(80,20,20)
end

th1_des = math.sin(t)
th2_des = math.pi/2 + math.cos(t)
th3_des = -math.cos(t)

U_F1 = saturate(pid_run(C1, 0.05, th1_des - X_th1),-10,10)
U_F2 = saturate(pid_run(C2, 0.05, th2_des - X_th2),-20,20)
U_F3 = saturate(pid_run(C3, 0.05, th3_des - X_th3),-10,10)

t = t + dt
