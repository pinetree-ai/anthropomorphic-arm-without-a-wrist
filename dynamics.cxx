#include "Dynamix.hxx"

#define PI 3.14159265358979323846
#define GAMMA_LEN 3

// NOTE: The armadillo linear algebra library is available. Use it!!! :)

// Output: Xdot [gammad, gammadd]
// Inputs: X [gamma, gammad]
//		   U (control inputs)
//		   params (loaded parameters) - access with params["<param name>"]

// Property tree access if needed
PropertyTree *props = PropertyTree::props();

double gz = 9.81;

mat H (GAMMA_LEN, GAMMA_LEN);
vec D (GAMMA_LEN);
vec G (GAMMA_LEN);
vec u (3);

vec gammad(GAMMA_LEN);
vec gammadd(GAMMA_LEN);

vec Xdot(GAMMA_LEN * 2);


vec dynamics_cxx(vec X, map<string, double> U, map<string, double> params, map<string, itable> tables)
{
	for (int i=0; i<GAMMA_LEN; i++) {
		Xdot(i) = X(GAMMA_LEN+i);
	}
	
	// Copy variables: generalized co-ordinates and derivatives
	double th1 = X(0);
	double th2 = X(1);
	double th3 = X(2);
	double th1d = X(3);
	double th2d = X(4);
	double th3d = X(5);
	
	// Copy variables: loaded parameters
	double frame1_m = params["frame1_m"];
	double frame1_Jzz = params["frame1_Jzz"];
	double frame1_b1 = params["frame1_b1"];
	double frame1_b2 = params["frame1_b2"];

	double frame2_m = params["frame2_m"];
	double frame2_rCMx = params["frame2_rCMx"];
	double frame2_Jyy = params["frame2_Jyy"];
	double frame2_b1 = params["frame2_b1"];
	double frame2_b2 = params["frame2_b2"];

	double frame3_m = params["frame3_m"];
	double frame3_rCMx = params["frame3_rCMx"];
	double frame3_Jyy = params["frame3_Jyy"];
	double frame3_rx = params["frame3_rx"];
	double frame3_b1 = params["frame3_b1"];
	double frame3_b2 = params["frame3_b2"];

	// DEFINE System Mass Matrix (H)
	H(0,0) = 1.0*frame1_Jzz*pow(sin(th1), 2) + 1.0*frame1_Jzz*pow(cos(th1), 2);
	H(0,1) = 0;
	H(0,2) = 0;
	H(1,0) = 0;
	H(1,1) = 1.0*frame2_Jyy*pow(sin(th2), 2) + 1.0*frame2_Jyy*pow(cos(th2), 2);
	H(1,2) = 0;
	H(2,0) = 0;
	H(2,1) = 0;
	H(2,2) = 1.0*frame3_Jyy*pow(sin(th3), 2) + 1.0*frame3_Jyy*pow(cos(th3), 2);
	
	// DEFINE Vector of Coriolis and centripetal forces
	// D is a vector of zeros
	D.zeros();

	// DEFINE Vector of Gravitational forces
	G(0) = 0;
	G(1) = frame2_m*frame2_rCMx*gz*cos(th2) - frame3_m*gz*(frame3_rCMx*(sin(th2)*sin(th3) - cos(th2)*cos(th3)) - frame3_rx*cos(th2));
	G(2) = -frame3_m*frame3_rCMx*gz*(sin(th2)*sin(th3) - cos(th2)*cos(th3));

	// Add Actuator friction parameters to D
	D(0) += frame1_b1*th1d + frame1_b2*sign(th1d);
	D(1) += frame2_b1*th2d + frame2_b2*sign(th2d);
	D(2) += frame3_b1*th3d + frame3_b2*sign(th3d);

	// DEFINE Vector of Inputs (u)
	u(0) = U["F1"];
	u(1) = U["F2"];
	u(2) = U["F3"];
	
	gammadd = solve(H, u - D - G);

	for (int i=0; i<GAMMA_LEN; i++) {
		Xdot(GAMMA_LEN+i) = gammadd(i);
	}
	
	return Xdot;
}
