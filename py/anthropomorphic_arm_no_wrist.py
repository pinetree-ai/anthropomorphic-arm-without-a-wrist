# Dynamix Model Definition for anthropomorphic arm without a wrist
# Authors: Narendran Muraleedharan <narendran.m@aptus.aero>
#		   Daniel Cohen <cohend2012@gmail.com>
#
# COPYRIGHT (c) 2019 Pinetree LLC & Aptus Engineering, Inc.

# Dependencies: sympy
import sympy as np

import sys
sys.path.append('../../../py')

from dynamics import *
from rotations import *
from sympy.utilities.codegen import codegen

# Constants
# Acceleration due to gravity
gz = np.symbols('gz')
g = np.transpose(np.Matrix([[0, 0, -gz]]))

## Setup generalized coordinates, velocities and accelerations as symbolic variables
# Generalized coordinates
th1 = np.symbols('th1')
th2 = np.symbols('th2')
th3 = np.symbols('th3')
gamma = np.Matrix([th1, th2, th3])

# Generalized velocities
th1d = np.symbols('th1d')
th2d = np.symbols('th2d')
th3d = np.symbols('th3d')
gammad = np.Matrix([th1d, th2d, th3d])

# Generalized accelerationsprint(G)
th1dd = np.symbols('th1dd')
th2dd = np.symbols('th2dd')
th3dd = np.symbols('th3dd')
gammadd = np.Matrix([th1dd, th2dd, th3dd])

# State vector and state vector rates
X = np.Matrix([th1, th2, th3, th1d, th2d, th3d])
Xd = np.Matrix([th1d, th2d, th3d, th1dd, th2dd, th3dd])


## Setup robot frames
# Base frame is placed with x-axis forward and z-axis up


# Frame 1 - rotate about the z-axis
# Mass
frame1_m = np.symbols('frame1_m')

# Center of mass offsets
frame1_rCM = np.zeros(3,1)

# Inertia tensor
frame1_J = np.zeros(3,3)
frame1_Jzz = np.symbols('frame1_Jzz')
frame1_J[2,2] = frame1_Jzz

# Translation from previous frame (base/inertial) to current frame
frame1_r = np.zeros(3,1)

# Rotation matrix from previous frame to current frame
frame1_T = rotz(th1)


# Frame 2 - rotate about the y-axis
# Mass
frame2_m = np.symbols('frame2_m')

# Vector of first mass moments
frame2_rCMx = np.symbols('frame2_rCMx')
frame2_rCM = np.transpose(np.Matrix([[frame2_rCMx, 0, 0]]))

# Inertia tensor
frame2_J = np.zeros(3,3)
frame2_Jyy = np.symbols('frame2_Jyy')
frame2_J[1,1] = frame2_Jyy

# Translation from previous frame to current frame
frame2_r = np.zeros(3,1)

# Rotation matrix from previous frame to current frame
frame2_T = roty(th2)


# Frame 3 - rotate about the y-axis
# Mass
frame3_m = np.symbols('frame3_m')

# Vector of first mass moments
frame3_rCMx = np.symbols('frame3_rCMx')
frame3_rCM = np.transpose(np.Matrix([[frame3_rCMx, 0, 0]]))

# Inertia tensor
frame3_J = np.zeros(3,3)
frame3_Jyy = np.symbols('frame3_Jyy')
frame3_J[1,1] = frame3_Jyy

# Translation from previous frame to current frame
frame3_rx = np.symbols('frame3_rx')
frame3_r = np.transpose(np.Matrix([[frame3_rx, 0, 0]]))

# Rotation matrix from previous frame to current frame
frame3_T = roty(th3)


## Compute system Kinetic and Potential energies
K = 0 # Start from 0
U = 0

# Frame 1
IIr1 = frame1_r
IIr1d = ChainDiff(frame1_r, X, Xd)
IT1 = frame1_T
IT1d = ChainDiff(frame1_T, X, Xd)

K += KineticEnergy(frame1_m, IIr1d, IT1d, frame1_rCM, frame1_J)
U += PotentialEnergy(frame1_m, IIr1, IT1, frame1_rCM, g)


# Frame 2
IIr2 = IIr1 + IT1 * frame2_r
IIr2d = ChainDiff(frame2_r, X, Xd)
IT2 = IT1 * frame2_T
IT2d = ChainDiff(frame2_T, X, Xd)

K += KineticEnergy(frame2_m, IIr2d, IT2d, frame2_rCM, frame2_J)
U += PotentialEnergy(frame2_m, IIr2, IT2, frame2_rCM, g)


# Frame 3
IIr3 = IIr2 + IT2 * frame3_r
IIr3d = ChainDiff(frame3_r, X, Xd)
IT3 = IT2 * frame3_T
IT3d = ChainDiff(frame3_T, X, Xd)

K += KineticEnergy(frame3_m, IIr3d, IT3d, frame3_rCM, frame3_J)
U += PotentialEnergy(frame3_m, IIr3, IT3, frame3_rCM, g)


## Compute H, D and G
# H: System mass matrix
H = np.transpose(np.Matrix([[K]]).jacobian(gammad)).jacobian(gammad)

# D: Vector of Coriolis and centripetal forces
D = np.transpose(np.Matrix([[K]]).jacobian(gammad)).jacobian(gamma) * gammad - np.transpose(np.Matrix([[K]]).jacobian(gamma))

# G: Vector of Gravitational forces
G = np.transpose(np.Matrix([[U]]).jacobian(gamma))


## Export hard-coded dynamics code
[(_, H_code), (_, _)] = codegen([("H", H)], "C99", header=False, empty=True)
print(H_code)

[(_, D_code), (_, _)] = codegen([("D", D)], "C99", header=False, empty=True)
print(D_code)

[(_, G_code), (_, _)] = codegen([("G", G)], "C99", header=False, empty=True)
print(G_code)